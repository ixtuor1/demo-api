# Inventx Praxisaufgabe 

## Ausgangslage 

Es existiert eine öffentliche JSON REST-API, welche uns eine Beispielliste von virtuellen Maschinen in unserer Cloud-Plattform liefert (auf "/virtualmachines"). Nachfolgend soll diese als "Plattform-API" bezeichnet werden. 

Ziel der Aufgabe ist es eine eigene JSON API mittels .NET, Java oder einer anderen Technologie zu schreiben, welche ihrerseits einen eigenen Endpunkt "/virtualmachines" anbietet. Nachfolgend soll diese als "Demo-API" bezeichnet werden. 


## Aufgabenstellung 
Erstelle in der Demo-API einen neuen Endpunkt "/virtualmachines". Dieser soll die Plattform-API aufrufen (GET auf /virtualmachines), anschliessend die erhaltenen VMs um das Feld "isPatchable" und "parentVMCount" erweitern und die erweiterten VMs dem Aufrufer der Demo-API als JSON zurückgeben. 

- Das Feld "isPatchable" soll dabei true sein, wenn laut aktuellem Datum (lokale Systemzeit der Demo-API) der dritte Tag im Monat ist und das Feld "status" der VM auf "Running" ist. Ansonsten soll "isPatchable" false sein.

- Das Feld "parentVMCount" soll die Anzahl der Eltern-VMs enthalten, die aufgrund des "parentId" Felds jeder VM ermittelt werden können. Hat zum Beispiel eine VM namens "TestVM" eine "parentId" von 5, die VM mit der ID 5 eine "parentId" von 1 und die VM mit der ID 1 eine "parentId" null, so soll das "parentVMCount"-Feld der "TestVM" 3 sein. Der Code zur Feststellung soll dabei selber geschrieben sein und nicht aus einer Library stammen.

- Mittels einem URL Parameter "status" soll ein VM-Status angegeben werden können, nach dem gesucht werden soll und bei dem der Endpunkt nur jene VMs zurückliefert, welche diesen Status haben.


Denke an Edge-Cases und mögliche Fehlerquellen und versuche diese sauber zu lösen.
Die Businesslogik soll wo du es für nötig erachtest mit Unittests versehen werden. Wir sollen erkennen, wie du generell Businesslogik mit UnitTests prüfst und welche Fälle dir dabei wichtig / prüfenswert erscheinen.
Die Änderungen am Code sollen als Merge-Request auf dem Gitlab-Repo erfasst werden. 
